# TP4 : DHCP



🌞 **Déterminer**
```
ipconfig /release
```
adresse du serveur dhcp : 10.33.79.254

 Bail obtenu : vendredi 27 octobre 2023 14:06:57

  Bail expirant : vendredi 27 octobre 2023 15:06:58

🌞 **Capturer un échange DHCP**

🌞 **Analyser la capture Wireshark**
```
la trame qui contient les informations proposées au client est la tram : OFFER
```

## II. Serveur DHCP

### 1. Topologie
🌞 **Preuve de mise en place**
```
[mathys@dhcp ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=22.5 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=11.3 ms

```
```
[mathys@node2 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=14.7 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=29.4 ms
```
```
[mathys@node2 ~]$ traceroute 64.233.160.0
traceroute to 64.233.160.0 (64.233.160.0), 30 hops max, 60 byte packets
gateway (10.4.1.254)  1.124 ms  2.513 ms  2.427 ms
10.0.3.2 (10.0.3.2)  4.847 ms  5.018 ms  4.355 ms
```

🌞 **Rendu**
```
[mathys@dhcp ~]$sudo nano /etc/resolv.confs
[mathys@dhcp ~]$ping ynov.com
[mathys@dhcp ~]$sudo dnf -y install dhcp-server
[mathys@dhcp ~]$sudo nano /etc/dhcp/dhcpd.conf
```
```
[mathys@dhcp ~]$ systemctl status dhcpd
● dhcpd.service - DHCPv4 Server Daemon
     Loaded: loaded (/usr/lib/systemd/system/dhcpd.service; enabled; preset: disabled)
     Active: active (running) since Sun 2023-11-05 00:15:26 CET; 5min 47s ago
       Docs: man:dhcpd(8)
             man:dhcpd.conf(5)
   Main PID: 1501 (dhcpd)
     Status: "Dispatching packets..."
      Tasks: 1 (limit: 4672)
     Memory: 4.6M
        CPU: 29ms
     CGroup: /system.slice/dhcpd.service
             └─1501 /usr/sbin/dhcpd -f -cf /etc/dhcp/dhcpd.conf -user dhcpd -group dhcpd --no-pid

```

```
[mathys@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
[sudo] password for mathys:

 Configuration serveur DHCP
 create new specify domain name
option domain-name     "tp4.dhcp";
 specify DNS server's hostname or IP address
option domain-name-servers     dlp.tp4.dhcp;
 default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.4.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.4.1.135 10.4.1.235;
    # specify broadcast address
    option broadcast-address 10.4.1.255;
    # specify gateway
    option routers 10.4.1.254;
}


```
🌞 **Test !**


🌞 **Prouvez que**

```
[mathys@node1 ~]$ ip a
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b2:de:47 brd ff:ff:ff:ff:ff:ff
    inet 10.4.1.135/24 brd 10.4.1.255 scope global dynamic noprefixroute enp0s3
```

🌞 **Bail DHCP serveur**
```
[mathys@node1 ~]$ cat /var/lib/dhcpd/dhcpd.leases
lease 10.4.1.135 {
  starts 6 2023/11/05 21:34:14;
  ends 0 2023/11/06 01:23:35;
  cltt 6 2023/11/05 21:34:14;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:b2:de:47;
  uid "\001\010\000'k]\343";
}

```

### 6. Options DHCP

🌞 **Nouvelle conf !**

```
[mathys@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf
# create new
# specify domain name
option domain-name     "tp4.dhcp";
# specify DNS server's hostname or IP address
option domain-name-servers     1.1.1.1;
# default lease time
default-lease-time 21600;
# max lease time
max-lease-time 24000;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.4.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.4.1.135 10.4.1.235;
    # specify broadcast address
    option broadcast-address 10.4.1.255;
    # specify gateway
    option routers 10.4.1.254;
}
```


🌞 **Test !**

🌞 **Capture Wireshark**
