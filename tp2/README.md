# TP2 : Ethernet, IP, et ARP



# I. Setup IP


🌞 **Mettez en place une configuration réseau fonctionnelle entre les deux machines**

connexion WIFI (partage de connexions )

```
ipconfig /all
```
 ip 1 :  192.168.158.75
 
 ip 2 :  192.168.158.36

Masque de sous-réseau : 255.255.255.0

adreses de reseaux : 192.168.158.0

adresse broadcast : 192.168.158.255

🌞 **Prouvez que la connexion est fonctionnelle entre les deux machines**

 ``` 
 ping  192.168.158.36

Envoi d’une requête 'Ping'  192.168.158.36 avec 32 octets de données :
Réponse de 192.168.158.36 : octets=32 temps=10 ms TTL=128
Réponse de 192.168.158.36 : octets=32 temps=7 ms TTL=128
Réponse de 192.168.158.36 : octets=32 temps=7 ms TTL=128
Réponse de 192.168.158.36 : octets=32 temps=8 ms TTL=128

 ```

🌞 **Wireshark it**

déterminez, grâce à Wireshark, quel type de paquet ICMP est envoyé par ping

pour le ping que vous envoyez :

 type 8 : (echo(ping)request)

et le pong que vous recevez en retour
:   

type 0 : (echo(ping)reply)


# II. ARP my bro


🌞 **Check the ARP table**

```
arp -a
```
mac binome : 00-a5-54-c6-91-d4

mac du gateway reseaux :      b2-aa-a0-76-3c-96


🌞 **Manipuler la table ARP**
 
 a executer en tant que administrateur 

 ```
 arp -d  
 arp a 

Interface : 10.33.65.206 --- 0x7
  Adresse Internet      Adresse physique      Type
  10.33.79.254          7c-5a-1c-d3-d8-76     dynamique
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.159.1 --- 0x9
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique

Interface : 192.168.166.1 --- 0xa
  Adresse Internet      Adresse physique      Type
  224.0.0.22            01-00-5e-00-00-16     statique
   ```
🌞 Wireshark it

```
adresses source et destination du ping : 
 
 (source 192.168.158.36 destination 192.168.158.75 )

 adresse source et destination pong : 
 
 (source 192.168.158.75 destination 192.168.158.36 )
```

adresse de destination : L'adresse destination sera l'adresse MAC de votre propre ordinateur.

adresse source : L'adresse source sera l'adresse MAC de la machine qui possède l'adresse IP recherchée.



# III. DHCP


🌞 **Wireshark it**

adresse qui change suite a nouveau partage  

D : Discover : (source: 0.0.0.0 destination 255.255.255.255
)

O : offer :  ( source  192.168.0.1 destination 192.168.0.10 ) 

R : request : (source  0.0.0.0 destination 255.255.255.255 ) 

A : ack source : ( 192.168.0.1 destination 192.168.0.10 ) 

        