# TP6 : Un LAN maîtrisé meo ?


🌞 **Dans le rendu, je veux**

```
[mathys@dns ~]$ sudo cat /etc/named.conf

//
// named.conf
//
// Provided by Red Hat bind package to configure the ISC BIND named(8) DNS
// server as a caching only nameserver (as a localhost DNS resolver only).
//
// See /usr/share/doc/bind*/sample/ for example named configuration files.
//

options {
        listen-on port 53 { 127.0.0.1; any; };
        listen-on-v6 port 53 { ::1; };
        directory       "/var/named";
        dump-file       "/var/named/data/cache_dump.db";
        statistics-file "/var/named/data/named_stats.txt";
        memstatistics-file "/var/named/data/named_mem_stats.txt";
        secroots-file   "/var/named/data/named.secroots";
        recursing-file  "/var/named/data/named.recursing";
        allow-query     { localhost; any; };
        allow-query-cache { localhost; any; };
        /*
         - If you are building an AUTHORITATIVE DNS server, do NOT enable recursion.
         - If you are building a RECURSIVE (caching) DNS server, you need to enable
           recursion.
         - If your recursive DNS server has a public IP address, you MUST enable access
           control to limit queries to your legitimate users. Failing to do so will
           cause your server to become part of large scale DNS amplification
           attacks. Implementing BCP38 within your network would greatly
           reduce such attack surface
        */
        recursion yes;

        dnssec-validation yes;

        managed-keys-directory "/var/named/dynamic";
        geoip-directory "/usr/share/GeoIP";

        pid-file "/run/named/named.pid";
        session-keyfile "/run/named/session.key";

        /* https://fedoraproject.org/wiki/Changes/CryptoPolicy */
        include "/etc/crypto-policies/back-ends/bind.config";
};

logging {
        channel default_debug {
                file "data/named.run";
                severity dynamic;
        };
};

# référence vers notre fichier de zone
zone "tp6.b1" IN {
     type master;
     file "tp6.b1.db";
     allow-update { none; };
     allow-query {any; };
};
# référence vers notre fichier de zone inverse
zone "1.4.10.in-addr.arpa" IN {
     type master;
     file "tp6.b1.rev";
     allow-update { none; };
     allow-query { any; };
};

```
```
[mathys@dns ~]$  sudo cat /var/named/tp6.b1.rev
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Reverse lookup
101 IN PTR dns.tp6.b1.
11 IN PTR john.tp6.b1.
```
```
[mathys@dns ~]$ sudo cat /var/named/tp6.b1.db
$TTL 86400
@ IN SOA dns.tp6.b1. admin.tp6.b1. (
    2019061800 ;Serial
    3600 ;Refresh
    1800 ;Retry
    604800 ;Expire
    86400 ;Minimum TTL
)

; Infos sur le serveur DNS lui même (NS = NameServer)
@ IN NS dns.tp6.b1.

; Enregistrements DNS pour faire correspondre des noms à des IPs
dns       IN A 10.6.1.101
john      IN A 10.6.1.11
```
```
[mathys@dns ~]$ sudo systemctl status named
● named.service - Berkeley Internet Name Domain (DNS)
     Loaded: loaded (/usr/lib/systemd/system/named.service; e>
     Active: active (running) since Tue 2023-11-21 11:23:07 C>
   Main PID: 12208 (named)
      Tasks: 6 (limit: 11009)
     Memory: 24.9M
        CPU: 105ms
     CGroup: /system.slice/named.service
             └─12208 /usr/sbin/named -u named -c /etc/named.c>

Nov 21 11:23:07 dns.tp6 named[12208]: configuring command cha>
Nov 21 11:23:07 dns.tp6 named[12208]: command channel listeni>
Nov 21 11:23:07 dns.tp6 named[12208]: configuring command cha>
Nov 21 11:23:07 dns.tp6 named[12208]: command channel listeni>
Nov 21 11:23:07 dns.tp6 named[12208]: managed-keys-zone: load>
Nov 21 11:23:07 dns.tp6 named[12208]: zone 1.4.10.in-addr.arp>
Nov 21 11:23:07 dns.tp6 named[12208]: zone tp6.b1/IN: loaded >
Nov 21 11:23:07 dns.tp6 named[12208]: all zones loaded
Nov 21 11:23:07 dns.tp6 named[12208]: running
Nov 21 11:23:07 dns.tp6 systemd[1]: Started Berkeley Internet>

```
```
[mathys@dns ~]$ ss -nl
tcp      LISTEN    0         10                                           10.6.1.101:53                     0.0.0.0:*
tcp      LISTEN    0         10                                           10.6.1.101:53                     0.0.0.0:*
```

🌞 **Ouvrez le bon port dans le firewall**

```
[mathys@dns ~]$ sudo firewall-cmd --add-port=53/udp --permanent
[sudo] password for mathys:
success
```
## 3. Test

🌞 **Sur la machine `john.tp6.b1`**

```
[mathys@john ~]$ dig google.com
;; ANSWER SECTION:
google.com.             35      IN      A       142.250.201.174
```
```
[mathys@john ~]$ nslookup dns.tp6.101
Server:         10.6.1.101
Address:        10.6.1.101#53

** server can't find dns.tp6.101: NXDOMAIN
```

🌞 **Sur votre PC**


```
PS C:\Users\Mathys> nslookup john.tp6.b1 10.6.1.101
Serveur :   UnKnown
Address:  10.6.1.101

Nom :    john.tp6.b1
Address:  10.6.1.11
```

🦈 **Capture `tp6_dns.pcapng`**


# III. Serveur DHCP

🌞 **Installer un serveur DHCP**
```
[mathys@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf

# create new
# specify domain name
option domain-name     "TP6_DHCP";
# specify DNS server's hostname or IP address
option domain-name-servers     10.6.1.101;
# default lease time
default-lease-time 21600;
# max lease time
max-lease-time 30000;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.6.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.6.1.13 10.6.1.37;
    # specify broadcast address
    option broadcast-address 10.6.1.255;
    # specify gateway
    option routers 10.6.1.254;
}
```


🌞 **Test avec `john.tp6.b1`**

 


🌞 **Requête web avec `john.tp6.b1`**



🦈 **Capture `tp6_web.pcapng`**

