
# tp7



🌞 **Effectuez une connexion SSH en vérifiant le fingerprint**

```
PS C:\Users\Mathys> ssh mathys@10.7.1.11
The authenticity of host '10.7.1.11 (10.7.1.11)' can't be established.
ED25519 key fingerprint is SHA256:rCVbJp56Y+FkkVpfKAP9bFSShPdUh6m9hALAzsjuMP8.
This host key is known by the following other names/addresses:
    C:\Users\Mathys/.ssh/known_hosts:1: 10.3.1.12
    C:\Users\Mathys/.ssh/known_hosts:4: 10.3.1.11
    C:\Users\Mathys/.ssh/known_hosts:5: 10.4.1.254
    C:\Users\Mathys/.ssh/known_hosts:6: 10.5.1.11
    C:\Users\Mathys/.ssh/known_hosts:7: 10.5.1.254
    C:\Users\Mathys/.ssh/known_hosts:8: 10.5.1.12
    C:\Users\Mathys/.ssh/known_hosts:9: 10.6.1.11
    C:\Users\Mathys/.ssh/known_hosts:10: 10.6.1.101
    (2 additional names omitted)
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '10.7.1.11' (ED25519) to the list of known hosts.
mathys@10.7.1.11's password:
Last login: Wed Dec 13 18:31:44 2023
[mathys@john ~]$
```
```
[mathys@john ~]$ ls /etc/ssh
moduli      ssh_config.d  sshd_config.d       ssh_host_ecdsa_key.pub  ssh_host_ed25519_key.pub  ssh_host_rsa_key.pub
ssh_config  sshd_config   ssh_host_ecdsa_key  ssh_host_ed25519_key    ssh_host_rsa_key
[mathys@john ~]$  sudo ssh-keygen -l -f /etc/ssh/ssh_host_ed25519_key
256 SHA256:rCVbJp56Y+FkkVpfKAP9bFSShPdUh6m9hALAzsjuMP8 /etc/ssh/ssh_host_ed25519_key.pub (ED25519)
```

🌞 **Consulter l'état actuel**

```
[mathys@routeur ~]$ ss -lnt
State  Recv-Q Send-Q   Local Address:Port   Peer Address:Port Process
LISTEN 0      128            0.0.0.0:22          0.0.0.0:*

LISTEN 0      128               [::]:22             [::]:*

```

🌞 **Modifier la configuration du serveur SSH**
```
# If you want to change the port on a SELinux system, you hav>
# SELinux about this change.
# semanage port -a -t ssh_port_t -p tcp #PORTNUMBER
#
Port 25000
#AddressFamily any
ListenAddress 10.7.1.254
#ListenAddress ::
```



🌞 **Prouvez que le changement a pris effet**

```
[mathys@routeur ~]$ ss -lnt
State  Recv-Q Send-Q  Local Address:Port    Peer Address:Port Process
LISTEN 0      128        10.7.1.254:25000        0.0.0.0:*

```

🌞 **N'oubliez pas d'ouvrir ce nouveau port dans le firewall**
```
[mathys@routeur ~]$ sudo firewall-cmd --add-port=25000/tcp --p
ermanent
[sudo] password for mathys:
success
```

🌞 **Effectuer une connexion SSH sur le nouveau port**
```
[mathys@routeur ~]$ ssh mathys@10.7.1.254 -p 25000
The authenticity of host '[10.7.1.254]:25000 ([10.7.1.254]:25000)' can't be established.
ED25519 key fingerprint is SHA256:rCVbJp56Y+FkkVpfKAP9bFSShPdUh6m9hALAzsjuMP8.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[10.7.1.254]:25000' (ED25519) to the list of known hosts.
mathys@10.7.1.254's password:
Last login: Wed Dec 13 19:09:12 2023 from 10.7.1.1
```
🌞 **Générer une paire de clés**
```
PS C:\Users\Mathys> ssh-keygen -t rsa -b 4096
Generating public/private rsa key pair.
Enter file in which to save the key (C:\Users\Mathys/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in C:\Users\Mathys/.ssh/id_rsa
Your public key has been saved in C:\Users\Mathys/.ssh/id_rsa.pub
The key fingerprint is:
SHA256:sUK4vU6tbZAkmH48GqbckKkMVzhG+f8gtVB+qs2kfPY mathys@mathyspc
The key's randomart image is:
+---[RSA 4096]----+
|   .             |
|  o  ..          |
| . =.o. .        |
|  * =++ .o       |
| oo+.*o=S        |
|.+= = O+         |
|=+o= Oo+.        |
|oo..+o=oo        |
|     oooE        |
+----[SHA256]-----+
```


🌞 **Déposer la clé publique sur une VM**

```
Mathys@mathyspc MINGW64 ~ (master)
$ ssh-copy-id mathys@10.7.1.11
/usr/bin/ssh-copy-id: INFO: Source of key(s) to be installed: "/c/Users/Mathys/.ssh/id_rsa.pub"
/usr/bin/ssh-copy-id: INFO: attempting to log in with the new key(s), to filter out any that are already installed
/usr/bin/ssh-copy-id: INFO: 1 key(s) remain to be installed -- if you are prompted now it is to install the new keys
mathys@10.7.1.11's password:

Number of key(s) added: 1

Now try logging into the machine, with:   "ssh 'mathys@10.7.1.11'"
and check to make sure that only the key(s) you wanted were added.


```

🌞 **Connectez-vous en SSH à la machine**

```
PS C:\Users\Mathys> ssh mathys@10.7.1.11
Last login: Wed Dec 13 20:12:09 2023 from 10.7.1.1
[mathys@john ~]$
```
🌞 **Supprimer les clés sur la machine `router.tp7.b1`**

```
[mathys@routeur ~]$ sudo rm /etc/ssh/ssh_host_*
[sudo] password for mathys:
[mathys@routeur ~]$
```
🌞 **Regénérez les clés sur la machine `router.tp7.b1`**
```
[mathys@routeur ~]$ sudo ssh-keygen -A
ssh-keygen: generating new host keys: RSA DSA ECDSA ED25519
[mathys@routeur ~]$ sudo systemctl restart sshd
[mathys@routeur ~]$
```

🌞 **Tentez une nouvelle connexion au serveur**

```
PS C:\Users\mathys> ssh mathys@10.7.1.11
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
```
```
PS C:\Users\Mathys> ssh mathys@10.7.1.254 -p 25000
The authenticity of host '[10.7.1.254]:25000 ([10.7.1.254]:25000)' can't be established.
ED25519 key fingerprint is SHA256:N7ucmgA1rxdwyFclbs+ddIJ5be+g73UKtgo/KW6U/WU.
This key is not known by any other names
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '[10.7.1.254]:25000' (ED25519) to the list of known hosts.
mathys@10.7.1.254's password:
Last login: Wed Dec 13 22:23:01 2023 from 10.7.1.1
[mathys@routeur ~]$
```

# III. Web sécurisé


🌞 **Montrer sur quel port est disponible le serveur web**
```
[mathys@web ~]$ ss -ln
tcp      LISTEN    0         511                                             0.0.0.0:67                     0.0.0.0:*
```

🌞 **Générer une clé et un certificat sur `web.tp7.b1`**

```
[mathys@web ~]$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
[mathys@web ~]$ sudo mv server.key /etc/pki/tls/private/web.tp7.b1.key
[mathys@web ~]$ sudo chown nginx:nginx /etc/pki/tls/private/web.tp7.b1.key
[mathys@web ~]$ sudo mv server.crt /etc/pki/tls/certs/web.tp7.b1.crt
[mathys@web ~]$ $ sudo chmod 0400 /etc/pki/tls/private/web.tp7.b1.key
 
```

🌞 **Modification de la conf de NGINX**

```
[mathys@web ~]$ sudo cat /etc/nginx/conf.d/webtp7.conf
server {

    listen 10.7.1.12:443 ssl;


    ssl_certificate /etc/pki/tls/certs/web.tp7.b1.crt;
    ssl_certificate_key /etc/pki/tls/private/web.tp7.b1.key;

    server_name www.webtp7;
    root /var/www/webtp7;
}
```

🌞 **Conf firewall**

```
[mathys@web ~]$ sudo firewall-cmd --add-port=443/tcp --permanent
success
[mathys@web ~]$ sudo firewall-cmd --reload
success

```

🌞 **Redémarrez NGINX**
```
[mathys@web ~]$ sudo systemctl restart nginx
succes
```


🌞 **Prouvez que NGINX écoute sur le port 443/tcp**

```
[mathys@web ~]$ ss -lnt
LISTEN 0      511        10.7.1.12:443       0.0.0.0:*
```

🌞 **Visitez le site web en https**

```
[mathys@john ~]$ curl -k https://10.7.1.12
<h1>web</h1>
```