# TP1 - Premier pas réseau

# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

**🌞 Affichez les infos des cartes réseau de votre PC**

```
ipconfig /all
```
- carte reseaux wifi sans fil :

00-45-E2-D2-91-C1 

10.33.48.153
- pas de port ethernet, pas de carte ethernet 

**🌞 Affichez votre gateway**
``` 
ip config /all
```
-  Passerelle par défaut : 10.33.51.254

**🌞 Déterminer la MAC de la passerelle**
```
ipconfig /all
 ping 10.33.51.254
 arp -a
```
- MAC de la passerelle : 7c-5a-1c-cb-fd-a4


**🌞 Trouvez comment afficher les informations sur une carte IP (change selon l'OS)**
```
 parametres>reseaux internet>wifi>propriétés du matériel 
 pannneau de configuration>centre réseaux et partage>connexions>details
```
- 10.33.48.153
- 00-45-E2-D2-91-C1,10.33.51.254

🌞 Utilisez l'interface graphique de votre OS pour **changer d'adresse IP** :

```
-pannneau de configuration>centre réseaux et partage>modifier les parametres avancées de la carte >wi-fi>propriétés>Protocole Internet version 4
```

🌞 **Il est possible que vous perdiez l'accès internet.** 

# II. Exploration locale en duo

pas de port ethernet suite du tp avec bjorn et lionel

🌞 **Modifiez l'IP des deux machines pour qu'elles soient dans le même réseau**

 ```
 Centre Réseau et partage > Connexion réseau > Choisir la carte réseau > Détails
 ```


🌞 **Vérifier à l'aide d'une commande que votre IP a bien été changée**

```
ipconfig /all
```

🌞 **Vérifier que les deux machines se joignent**

```
Envoi d’une requête 'Ping' 10.10.10.213 avec 32 octets de données :

Réponse de 10.10.10.213 : octets=32 temps=4 ms TTL=128

Réponse de 10.10.10.213 : octets=32 temps=4 ms TTL=128

Réponse de 10.10.10.213 : octets=32 temps=4 ms TTL=128

Réponse de 10.10.10.213 : octets=32 temps=5 ms TTL=128
```


🌞 **Déterminer l'adresse MAC de votre correspondant**

```
arp -a
```

Adresse MAC: 08-8f-c3-fe-ea-ee

## 4. Petit chat privé


🌞 **sur le PC *serveur*** 
[nc.exe]

```
TCP 10.33.48.39:139 0.0.0.0:0 LISTENING
```

🌞 **sur le PC *client*** 


🌞 **Visualiser la connexion en cours**

```
PS C:\Users\fayer> netstat -a -n -b

   [nc64.exe]
  TCP    10.33.48.141:139       0.0.0.0:0              LISTENING 

```


🌞 **Pour aller un peu plus loin**

```
PS C:\Users\fayer> netstat -a -n -b
[nc64.exe]
  TCP    0.0.0.0:27036          0.0.0.0:0              LISTENING

```
```
PS C:\Users\fayer> netstat -a -n -b
 [nc64.exe]
  TCP    10.33.48.141:139       0.0.0.0:0              LISTENING

```




## 5. Firewall

🌞 **Activez et configurez votre firewall**
 
 
```
PS C:\Users\lione\Downloads\netcat-win32-1.11\netcat-1.11> ping 192.168.137.1

Envoi d’une requête 'Ping'  192.168.137.1 avec 32 octets de données :
Réponse de 192.168.137.1 : octets=32 temps=8 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=4 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=4 ms TTL=128
Réponse de 192.168.137.1 : octets=32 temps=4 ms TTL=128

PS C:\Users\fayer\Desktop\netcat-win32-1.11\netcat-1.11>.\nc64.exe -l -p 8000
vghvghvghj
salut

```

  
## 6. Utilisation d'un des deux comme gateway


🌞**Tester l'accès internet**

```
PS C:\Users\lione\Downloads\netcat-win32-1.11\netcat-1.11> ping 1.1.1.1

Envoi d’une requête 'Ping'  1.1.1.1 avec 32 octets de données :
Réponse de 192.168.137.2 : Impossible de joindre l’hôte de destination.
Délai d’attente de la demande dépassé.
Réponse de 192.168.137.2 : Impossible de joindre l’hôte de destination.
Réponse de 192.168.137.2 : Impossible de joindre l’hôte de destination.

Statistiques Ping pour 1.1.1.1:
    Paquets : envoyés = 4, reçus = 3, perdus = 1 (perte 25%)

```
🌞 **Prouver que la connexion Internet passe bien par l'autre PC**

```
pas réussis 
```

# III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

🌞**Exploration du DHCP, depuis votre PC**

```
ipconfig /all
```

- l'adresse IP du serveur DHCP du réseau WiFi YNOV : 10.33.51.254
-  Bail expirant : jeudi 12 octobre 2023 13:31:44
## 2. DNS

🌞Trouver l'adresse IP du serveur DNS que connaît votre ordinateur

```
nslookup
```

 Serveurs DNS : 10.33.10.2

🌞 Utiliser, en ligne de commande l'outil `nslookup` (Windows, MacOS) ou `dig` (GNU/Linux, MacOS) pour faire des requêtes DNS à la main

```
nslookup google.com 8.8.8.8
```
  - pour `google.com` :   142.250.179.110
  - pour `ynov.com` : 

  reverse lookup : 

  ```
  nslookup 231.31.113.12 8.8.8.8

  nslookup 78.34.2.17 8.8.8.8
  ```
  - pour 231.31.113.12:   ne parvient pas a trouver 

  - pour 78.34.2.17 :  78.34.2.17

## IV. Wireshark

🌞 Utilisez le pour observer les trames qui circulent entre vos deux carte Ethernet. Mettez en évidence 