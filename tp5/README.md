# TP5 : TCP, UDP et services réseau

🌞 **Déterminez, pour ces 5 applications, si c'est du TCP ou de l'UDP**
 
 ```
 netflix : ip destination : 62.39.148.63, port dest : 443, port local 61609

telegram : ip destination : 149.154.167.43, port dest : 443, port lacal : 62185

youtube : ip destination : 91.68.245.14, port dest : 443, port local : 60698

telegram: ip destination : 92.122.188.4, port dest : 443, port local : 62451

dalymotion : ip destination 188.65.126.8, port dest 443, port local: 59523
 ```

🌞 **Demandez l'avis à votre OS**


## 1. SSH

🌞 **Examinez le trafic dans Wireshark**


🌞 **Demandez aux OS**
```
PS C:\Users\Mathys> netstat -i -n
TCP    10.5.1.1:57965         10.5.1.11:22           ESTABLISHED        62488

[mathys@localhost ~]$ ss -a
tcp     ESTAB    0        52                                          10.5.1.11:ssh                        10.5.1.1:57965
```


🦈 **`tp5_3_way.pcapng` : une capture clean avec le 3-way handshake, un peu de trafic au milieu et une fin de connexion**

## 2. Routage


🌞 **Prouvez que**
```
[mathys@node1 ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=16.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=14.1 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=115 time=13.9 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=115 time=16.3 ms

[mathys@node1 ~]$ ping www.ynov.com
PING www.ynov.com (104.26.11.233) 56(84) bytes of data.
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=1 ttl=55 time=30.2 ms
64 bytes from 104.26.11.233 (104.26.11.233): icmp_seq=2 ttl=55 time=22.2 ms
^C64 bytes from 104.26.11.233: icmp_seq=3 ttl=55 time=45.3 ms
```

## 3. Serveur Web


🖥️ **Machine `web.tp5.b1`**

🌞 **Installez le paquet `nginx`**

```
 sudo dnf install nginx
[sudo] password for mathys:
Rocky Linux 9 - BaseOS                                                                               389  B/s | 4.1 kB     00:10
Rocky Linux 9 - BaseOS                                                                               120 kB/s | 1.9 MB     00:15
Rocky Linux 9 - AppStream                                                                            438
```


🌞 **Créer le site web**
```
[mathys@web ~]$ sudo mkdir /var/www/
[mathys@web ~]$ sudo mkdir /var/www/site_web_nul
[mathys@web ~]$ sudo nano /var/www/site_web_nul
```


🌞 **Donner les bonnes permissions**
```
[mathys@web ~]$ sudo chown -R nginx:nginx /var/www/site_web_nul
```

🌞 **Créer un fichier de configuration NGINX pour notre site web**

```
[mathys@web ~]$ cd
[mathys@web ~]$ cd /etc/nginx/
[mathys@web nginx]$ sudo nano /conf.d
```

🌞 **Démarrer le serveur web !**


🌞 **Ouvrir le port firewall**
```
[mathys@web nginx]$ sudo firewall-cmd --permanent --zone=public --add-service=http
success
[mathys@web nginx]$ sudo firewall-cmd --reload
success
```

🌞 **Visitez le serveur web !**
```
[mathys@web nginx]$ sudo curl http://10.5.1.12
[sudo] password for mathys:
<h1>Coucou</h1>
```


🌞 **Visualiser le port en écoute**

[mathys@web conf.d]$ ss -nlt
State         Recv-Q        Send-Q               Local Address:Port               Peer Address:Port       Process
LISTEN        0             511                        0.0.0.0:80                      0.0.0.0:*
LISTEN        0             511                           [::]:80                         [::]:*


🌞 **Analyse trafic**

fais sur powershell pas de fin/Ack

