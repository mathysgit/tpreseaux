# I. ARP

## 1. Echange ARP

🌞**Générer des requêtes ARP**
```
arp a
```
```
[mathys@localhost ~]$ ip neigh show
10.3.1.11 dev enp0s3 lladdr 08:00:27:9f:20:51 STALE
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:3b DELAY

[mathys@localhost ~]$ ip neigh show
10.3.1.1 dev enp0s3 lladdr 0a:00:27:00:00:3b DELAY
10.3.1.12 dev enp0s3 lladdr 08:00:27:76:4f:bc STALE
```

## 2. Analyse de trames

🌞**Analyse de trames**

```
commande sudo tcpdumn

15:27:10.561648 IP localhost.localdomain.ssh > 10.3.1.1.49984: Flags [P.], seq 60:256, ack 1, win 501, length 196
 
```
```
 sudo ip neigh flush all
```

🌞**Ajouter les routes statiques nécessaires pour que `john` et `marcel` puissent se `ping`**


```
[mathys@localhost ~]$ ip route show
10.3.1.0/24 dev enp0s3 proto kernel scope link src 10.3.1.11 metric 100
10.3.2.0/24 via 10.3.1.254 dev enp0s3

```
```
[mathys@localhost ~]$ping 10.3.2.12
PING 10.3.2.12 (10.3.2.12) 56(84) bytes of data.
64 bytes from 10.3.2.12: icmp_seq=1 ttl=63 time=1.43 ms
64 bytes from 10.3.2.12: icmp_seq=2 ttl=63 time=1.61 ms
64 bytes from 10.3.2.12: icmp_seq=3 ttl=63 time=1.45 ms
64 bytes from 10.3.2.12: icmp_seq=4 ttl=63 time=1.26 ms
64 bytes from 10.3.2.12: icmp_seq=5 ttl=63 time=1.26 ms
```

## 2. Analyse de trames

🌞**Analyse des échanges ARP**


| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
| ----- | ----------- | --------- | ------------------------- | -------------- | -------------------------- |
| 1     | Requête ARP | x         | 08:00:27:76:4f:bc         | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | x         | 08:00:27:9f:20:51         | x              | 08:00:27:76:4f:bc          |
| ...   | ...         | ...       | ...                       |                |                            |
| ?     | Ping        | 10.3.2.12 | 08:00:27:76:4f:bc         | 10.3.2.11      | 08:00:27:9f:20:51          |
| ?     | Pong        | 10.3.2.11 | 08:00:27:9f:20:51         | 10.3.2.12      | 08:00:27:76:4f:bc          |


## 3. Accès internetgi

🌞**Donnez un accès internet à vos machines** 

```
Depuis le routeur :
[mathys@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=17.5ms 
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=20.7ms 
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=18.8ms 
```
🌞**Donnez un accès internet à vos machines**
```
commande pour jhon :

ip route add default via 10.3.1.254 dev enp0s3

pour marcel :
ip route add default via 10.3.2.254 dev enp0s8
```
```
Depuis Jhon :
[mathys@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=113 time=18.6 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=113 time=21.2 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=113 time=19.1 ms
```
```
Depuis Jhon :
[mathys@localhost ~]$ ping google.com
PING google.com (142.250.179.110) 56(84) bytes of data.
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=1 ttl=115 time=22.4 ms
64 bytes from par21s20-in-f14.1e100.net (142.250.179.110): icmp_seq=2 ttl=115 time=18.4 ms
^C64 bytes from 142.250.179.110: icmp_seq=3 ttl=115 time=19.5 ms

--- google.com ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 10097ms
rtt min/avg/max/mdev = 18.434/20.115/22.422/1.687 ms
```

🌞**Analyse de trames**

