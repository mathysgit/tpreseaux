# Hangman Web

![HTTPS](https://th.bing.com/th/id/OIP.T3_B25FnaWgRKfOQJ-Bi1wHaJ4?w=197&h=263&rs=1&pid=ImgDetMain)


## Sommaire 

  - [ le projet ](#1)
  - [comment utiliser le hangman](#2)
  - [regles du jeu ](#3)
 - [fichier ](#4)


## le projet

Le projet Hangman Web est un projet dont le but est de recréer le jeu du pendu sur une interface web en utilisant un langage imposé : le Golang.

Nous avons choisi de suivre de crée une interface web qui nous représente et qui reste en accord avec une interface de jeu classique.


## comment utiliser le hangman

Pour lancer le projet, vous avez seulement besoin de cloner ou télécharger ce repository, ouvrir un terminal et vous diriger dans le dossier contenant le projet. Ici, vous n'avez plus qu'à exécuter la commande go run main.go, et utiliser votre navigateur pour vous rendre sur le lien suivant : http://localhost:8080.

## règles du jeu 

Deviner toute les lettres qui doivent composer un mot, éventuellement avec un nombre limité de tentatives (ici 10).

## fichier 

le projet se compose en plusieur fichier distinct c'est a dire :

main.go : C'est notre fichier main, il nous sert a éffectuer tout le nécessaire pour que le programme fonctionne.

hangmanbuild.txt : Ce fichier contient les dessins du pendu pour chaque tentative, fichier qui est scanner et print par le PrintHangman.go

words.txt : Ce fichier contient la liste de mot que le programme peut choisir aléatoirement a faire deviner grace au main.go.

index.html et hangman.css : ces fichier constituent le front-end de notre projet c'est a dire notre interface web sur laquel notre programme sera lancée.